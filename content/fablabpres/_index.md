+++
title = "Fablab presentation"
outputs = ["Reveal"]
[reveal_hugo]
custom_theme = "benou_black.scss"
custom_theme_compile = true
[logo]
src = "../img/fablogo_w.svg"
width = "10%" # Size of the file.
diag = "2%" # How far from the top right should the file be.
+++

# Des bases d'électronique


