+++
weight = 2
+++

{{% section %}}

# Qu'est-ce qu'on va apprendre ?

Comprendre comment les bases de l'éléctricité !

---

## Est-ce que c'est clair ce schéma électrique ?

{{< svg "static/img/circ/out/source_res_led.svg" >}}


---

## Ma spécialité ?


{{< svg "static/img/circ/out/source_res_led_realcomp.svg" >}}



{{% /section %}}



{{% section %}}

## L'amplificateur opérationnel

C'est tout simplement de la magie !

Sans lui la musique techno n'éxisterait pas.

---

## Réduire l'amplitude/volume c'est facile ?

{{< svg "static/img/circ/out/source_res_led.svg" >}}


---

## Augmenter l'amplitude/volume c'est plus difficile ?

{{< svg "static/img/circ/out/op_amp_expl.svg" >}}


{{% /section %}}