+++
title = "Template presentation"
outputs = ["Reveal"]
[reveal_hugo]
custom_theme = "benou_black.scss"
custom_theme_compile = true
[logo]
src = "img/logo_w.svg"
+++

## Template Example

---

This presentation shows how to take advantage of reveal-hugo's slide template feature.

---


{{< slide background-video="https://ia600209.us.archive.org/20/items/ElephantsDream/ed_hd_512kb.mp4">}}


---

## Hello

Here's an example of using a template called `blue`, defined in the front matter of this presentation's `_index.md` file.

---

The template can contain any valid slide customization params:

```toml
[reveal_hugo.templates.blue]
background = "#0011DD"
transition = "zoom"
```

---

Then add it to any slide using the slide shortcode:

```
{{</* slide template="blue" */>}}
```

